<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlapak extends CI_Model {

	function threadAll(){
		$thread = $this->db->query("SELECT * FROM threadAll");
		return $thread->result_array();
	}
	
	function threadKaskus($id){
		$thread = $this->db->query("SELECT * FROM threadAll WHERE id = '$id'");
		return $thread->result_array();
	}
	
	function threadPost($id){
		$thread = $this->db->query("SELECT * FROM cek_reply WHERE id_thread = '$id'");
		return $thread->result_array();
	}
	
	function threadSatu($id){
		$thread = $this->db->query("SELECT * FROM cek_reply WHERE id_tr = '$id'");
		return $thread->result_array();
	}
	
	function getUser($id){
			return $this->db->get_where('user_kaskus', array('user_id'=>$id))->result_array();		
	}
	
	function allUser(){
		$semua = $this->db->query("SELECT * from user_kaskus");
		return $semua->result_array();
	}
	
	function saringid($id){
		$max = $this->db->query("SELECT post_id from cek_reply where post_id = '$id' ");
		return $max->result_array();
	}
	
	function savethread($id_thread, $username, $user_id, $tanggal, $isipost, $post_id, $flag){
		$simpan = $this->db->query("INSERT IGNORE INTO cek_reply (id_thread, username, user_id, tanggal, isipost, post_id, flag, stat) VALUES ('$id_thread', '$username', '$user_id', '$tanggal', '$isipost', '$post_id', '$flag', 0)");
		return $simpan;		
	}
	
	function saveLapak($thread, $ket, $pemilik){
		$simpan = $this->db->query("INSERT IGNORE INTO threadAll (thread, keterangan, pemilik) VALUES ('$thread', '$ket', '$pemilik')");
		return $simpan;		
	}
	
	function pengguna(){
		$semua = $this->db->query("SELECT * FROM  user_kaskus");
		return $semua->result_array();
	}
	
	public function random_isi($id){
		$random = $this->db->query("SELECT id, sundul_lapak_isi FROM sunduLapak WHERE sundul_lapak_status = 0 AND forLapak='$id' ORDER BY RAND() LIMIT 1");
		return $random->result_array();
	}
	
	function smile(){
		$random = $this->db->query("SELECT smile FROM smile ORDER BY RAND()");
		return $random->result_array();
	}
	
	function kehabisan_kata($id){
		$habis = $this->db->query("SELECT * FROM sunduLapak WHERE sundul_lapak_status = 0 AND forLapak='$id'");
		return $habis->result_array();
	}
	
	function relod_ucapan($id){
		$update = $this->db->query("update sunduLapak set sundul_lapak_status = 0 where forLapak='$id'");
		return $update;
	}
	
	function updateStatus($id){
			$data = array(
				'sundul_lapak_status'=> 1
			);
			$this->db->where('id', $id);
			$this->db->update('sunduLapak', $data);
	}
	
	function sundulan($id){
		$sundul = $this->db->query("SELECT * FROM sunduLapak WHERE forLapak ='$id'");
		return $sundul->result_array();
	}
	
	function editsundulan($id){
		$sundul = $this->db->query("SELECT * FROM sunduLapak WHERE id='$id'");
		return $sundul->result_array();
	}
	
	function edit($id){
		$data = array(
			'sundul_lapak_isi'=> $this->input->post('sundulan'),
		);
		$this->db->where('id', $id);
		$this->db->update('sunduLapak', $data);
	}
	
	function add($id){
		$st = array(
			'sundul_lapak_isi' => $this->input->post('sundulan'),
			'sundul_lapak_status' => 0,
			'forLapak' => $id,
        );
        $this->db->insert('sunduLapak', $st);
	}
	
	function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('sunduLapak');
	}
}