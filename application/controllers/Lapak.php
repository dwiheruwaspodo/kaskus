<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//require_once APPPATH.'controllers/Functions.php';

class Lapak extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model('M_kaskus_jokes', 'jokes', TRUE);
		//$this->load->model('M_lapak', 'lapak', TRUE);
		$this->load->model('Mlapak', 'jual', TRUE);
		$this->load->helper('date');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('email');
		
		if($this->session->userdata('login') == ''){
            redirect('otomasi');
        }
	}
	
	function login($username, $pass){
		
		$loginURL = 'https://www.kaskus.co.id/user/login/';
		
		$page = $this->functions->curl($loginURL);
	
		//$this->functions->debug($page);
		
		$post = array();
		$post['securitytoken'] = $this->functions->cut_str($page, 'name="securitytoken" value="', '"');
		$post['url'] = '/user/login/';
		$post['md5password'] = md5($pass);
		$post['md5password_utf'] = md5($pass);
		$post['username'] = $username;
		$post['password'] = '';
		$post['rememberme'] = 'rememberme';
		
		
		$page = $this->functions->curl($loginURL, 0, $post, $loginURL);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		$page = $this->functions->curl('https://www.kaskus.co.id/');
		//$this->functions->debug($page);
		
		$simpancook = $this->kaskus_model->saveCookie($username, $cookie);
		return $cookie;
	}
	
	/* function tampil(){
		$data = $this->jual->threadAll();
		
		print_r($data);
	} */
	
	
	function index(){
		$data['thread'] = $thread = $this->jual->threadAll();
		$this->load->view('lapak/lapaklapak', $data);
	}
	
	function fjb($id){
		$data['thread'] = $thread = $this->jual->threadAll();
		$th = $this->jual->threadKaskus($id);
		$data['judul'] = "Lapak ".$th[0]['keterangan'];
		$data['link'] = $th[0]['thread'];
		$data['isi'] = 'lapak/composesundul';
		$data['pengirim'] = $this->jual->pengguna();
		$data['action'] = base_url().'lapak/sendsundul/'.$id.'/';
		$data['maut'] = 'http://cloudmyfile.com/newkaskus/lapak/sundulmaut/'.$id;
		
		
		//otomatis
		date_default_timezone_set('Asia/Bangkok');
		$salam = $this->salam();
		$smile = $this->jual->smile();
		$rand_isi = $this->jual->random_isi($th[0]['id']);
		$kuota_kata = $this->jual->kehabisan_kata($th[0]['id']);
		
		if(empty($kuota_kata)||empty($rand_isi)){
			$this->jual->relod_ucapan($th[0]['id']);
			$data['pesan'] = "Kata-kata kosong";
		}
		else{
			$idsun = $rand_isi[0]['id'];
			$rand = rand(0,19);
			$sundulisi = $salam.", ".$rand_isi[0]['sundul_lapak_isi']."".$smile[$rand]['smile'];
			$data['pesan'] = $sundulisi;
			$data['sumon'] = base_url().'lapak/nyundul/'.$id.'/'.$idsun.'/';
		}
		
		/* --- nav --- */
		$data['wall'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/postThread/'.$id.'">Thread</a></li>';
		$data['compose'] = '<li class="active"><a href="http://cloudmyfile.com/newkaskus/lapak/fjb/'.$id.'">Sundul</a></li>';
		$data['management'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/tambahsundul/'.$id.'">Manajemen Otomatis</a></li>';
		
		$this->load->view('lapak/sundul', $data);
	}
	
	function sendsundul($id, $pengirim){
		$th = $this->jual->threadKaskus($id);
		$user = $this->jokes->getUser($pengirim);
		
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$link = "http://fjb.kaskus.co.id/post_reply/".$th[0]['thread'];
		$page = $this->functions->curl($link, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($link, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
	
		//ambil token
		$token = $this->functions->cut_str($page, '<input type="hidden" name="securitytoken" value="', '"');
		
		unset($post);
		$post = array();
		$post['securitytoken'] = $token;
		$post['title'] = '';
		$post['message'] = $this->input->post('message');
		$post['ajaxhref'] = '/misc/getsmilies/';
		$post['forumimg'] = '';
		$post['parseurl'] = 1;
		$post['emailupdate'] = '9999';
		$post['folderid'] = 0;
		$post['rating'] = 0;
		$post['sbutton'] = 'Submit Reply';
		
		//send sundul
		$page = $this->functions->curl($link, $cookie, $post, $link);
		redirect('lapak/fjb/'.$id);
	}
	
	
	function nyundul($id, $idsun, $pengirim){
		$th = $this->jual->threadKaskus($id);
		$user = $this->jokes->getUser($pengirim);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$link = "http://fjb.kaskus.co.id/post_reply/".$th[0]['thread'];
		$ref = "http://fjb.kaskus.co.id/product/".$th[0]['thread'];
		
		$page = $this->functions->curl($ref, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//$this->functions->debug($page);
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($ref, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		//ambil token
		$token = $this->functions->cut_str($page, '<input type="hidden" name="securitytoken" value="', '"');
		
		unset($post);
		$post = array();
		$post['securitytoken'] = $token;
		$post['title'] = '';
		$post['message'] = $this->input->post('pesan');
		$post['ajaxhref'] = '/misc/getsmilies/';
		$post['forumimg'] = '';
		$post['parseurl'] = 1;
		$post['emailupdate'] = '9999';
		$post['folderid'] = 0;
		$post['rating'] = 0;
		$post['sbutton'] = 'Submit Reply';
		
		//send sundul
		$page = $this->functions->curl($link, $cookie, $post, $ref);
		
		//updatekata
		$this->jual->updateStatus($idsun);
		
		redirect('lapak/fjb/'.$id);
	}
	
	public function salam(){
		date_default_timezone_set('Asia/Bangkok');
		$jam = date('H');
		
		if($jam >= 19 ){
			$rand_waktu = rand(0, 2);
			if($rand_waktu == 0){
				return "selamat malaaaammm";
			}
			elseif($rand_waktu == 1){
				return "maleeeem,gan... ";
			}
			else{
				return "maleeeemmm...";
			}
		}
		elseif($jam >= 16){
			$rand_waktu = rand(0, 2);
			if($rand_waktu == 0){
				return "selamat soreeeee";
			}
			elseif($rand_waktu == 1){
				return "sore,gan... ";
			}
			else{
				return "soree...";
			}
		}
		elseif($jam >= 11){
			$rand_waktu = rand(0, 2);
			if($rand_waktu == 0){
				return "selamat siang... ";
			}
			elseif($rand_waktu == 1){
				return "siang,gan... ";
			}
			else{
				return "siang... ";
			}
		}
		else{
			$rand_waktu = rand(0, 2);
			if($rand_waktu == 0){
				return "selamat pagiiiiiiiii";
			}
			elseif($rand_waktu == 1){
				return "pagiiii,gan... ";
			}
			else{
				return "morniiiinnnggg...";
			}
		}	
	}
	
	function tambahsundul($id){
		$th = $this->jual->threadKaskus($id);
		$data['thread'] = $thread = $this->jual->threadAll();
		
		$data['sundul'] = $this->jual->sundulan($id);
		$data['isi'] = 'lapak/tambahsundul';
		$data['judul'] = "Lapak ".$th[0]['keterangan'];
		$data['link'] = $th[0]['thread'];
		$data['keterangan'] = $th[0]['keterangan'];
		
		$data['reset'] = "http://cloudmyfile.com/newkaskus/lapak/reset/".$id;
		$data['simpan'] = "http://cloudmyfile.com/newkaskus/lapak/simpanSundul/".$id;
		$data['edit'] = "http://cloudmyfile.com/newkaskus/lapak/editsundul/".$id;
		$data['hapus'] = "http://cloudmyfile.com/newkaskus/lapak/delete/".$id;
		
		/* --- nav --- */
		$data['wall'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/postThread/'.$id.'">Thread</a></li>';
		$data['compose'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/fjb/'.$id.'">Sundul</a></li>';
		$data['management'] = '<li class="active"><a href="http://cloudmyfile.com/newkaskus/lapak/tambahsundul/'.$id.'">Manajemen Otomatis</a></li>';
		
		$this->load->view('lapak/sundul', $data);
		
	}
	
	function simpanSundul($id){
		$this->jual->add($id);
		$this->session->set_flashdata('success', 'Data berhasil disimpan');
		redirect('lapak/tambahsundul/'.$id);
	}
	
	function editsundul($thr, $id){
		$th = $this->jual->threadKaskus($thr);
		$data['thread'] = $thread = $this->jual->threadAll();
		
		//$data['id'] = $id;
		$data['sundulan'] = $this->jual->editsundulan($id);
		$data['judul'] = "Lapak ".$th[0]['keterangan'];
		$data['link'] = $th[0]['thread'];
		$data['isi'] = 'lapak/editsundulan';
		$data['edit'] = "http://cloudmyfile.com/newkaskus/lapak/edit/".$thr."/".$id;
		//$data['thr'] = $thr;
		
		/* --- nav --- */
		$data['wall'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/postThread/'.$thr.'">Thread</a></li>';
		$data['compose'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/fjb/'.$thr.'">Sundul</a></li>';
		$data['management'] = '<li class="active"><a href="http://cloudmyfile.com/newkaskus/lapak/tambahsundul/'.$thr.'">Manajemen Otomatis</a></li>';
		
		$this->load->view('lapak/sundul', $data);
	}
	
	function edit($thr, $id){
		$this->jual->edit($id);
		$this->session->set_flashdata('success', 'Data berhasil diperbarui');
		redirect('lapak/tambahsundul/'.$thr);
	}
	
	function delete($id){
		$this->jual->delete($id);
		$this->session->set_flashdata('success', 'Data berhasil dihapus');
		
	}
	
	function reset($id){
		$this->jual->relod_ucapan($id);
		$this->session->set_flashdata('success', 'Status berhasil diperbarui');
		redirect('lapak/tambahsundul/'.$id);
	}
	
	function sundulmaut($id){
		$th = $this->jual->threadKaskus($id);
		$pengirim = $th[0]['pemilik'];
		$link = "https://fjb.kaskus.co.id/fjb/sundul";
		$user = $this->jokes->getUser($pengirim);
		
		$ref = "http://fjb.kaskus.co.id/product/".$th[0]['thread'];
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$page = $this->functions->curl($ref, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "Sign in | Join")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($ref, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		//ambil token
		$token = $this->functions->cut_str($page, '<input type="hidden" name="securitytoken" value="', '"');
		//ambil pos_id
		$id_post = $this->functions->cut_str($page, "<input id='post_id' type='hidden' value=\"", '"');
		
		unset($post);
		$post = array();
		$post['securitytoken'] = $token;
		$post['post_id'] = $id_post;
		
		/* print_r($post);
		$this->functions->debug($ref);
		$this->functions->debug($link);
		$this->functions->debug($cookie); */
		
		//send sundul
		$page = $this->functions->curl($link, $cookie, $post, $ref);
		//$this->functions->debug($page);
		redirect('lapak/fjb/'.$id);
		
	}
	
	function postThread($id){
		$th = $this->jual->threadKaskus($id);
		$data['thread'] = $thread = $this->jual->threadAll();
		$data['fjb'] = $this->jual->threadPost($id);
		$data['judul'] = "Lapak ".$th[0]['keterangan'];
		$data['link'] = $th[0]['thread'];
		$data['pengirim'] = $this->jual->pengguna();
		$data['isi'] = 'lapak/thread';
		$data['thr'] = $id;
		//$data['action'] = 'http://cloudmyfile.com/newkaskus/lapak/replyLapak/'.$id;
		
		/* --- nav --- */
		$data['wall'] = '<li class="active"><a href="http://cloudmyfile.com/newkaskus/lapak/postThread/'.$id.'">Thread</a></li>';
		$data['compose'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/fjb/'.$id.'">Sundul</a></li>';
		$data['management'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/tambahsundul/'.$id.'">Manajemen Otomatis</a></li>';
		
		$this->load->view('lapak/sundul', $data);
	}
	
	public function quotes($idcol, $idpos, $id, $idpembalas){
		$th = $this->jual->threadKaskus($id);
		$thread = $th[0]['thread'];
		
		$data['thread'] = $this->jual->threadAll();
		
		$quotes = "http://fjb.kaskus.co.id/post_reply/".$thread."/?post=".$idpos;
		$user = $this->jokes->getUser($idpembalas);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$page = $this->functions->curl($quotes , $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($quotes , $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		$pesan = $this->functions->cut_str($page, '<textarea name="message" id="reply-messsage" onfocus="countChars()" onkeydown="countChars()" onkeyup="countChars()" class="markItUpEditor">', '</textarea>');

		$data['sundul'] = $this->jual->threadSatu($idcol);
		$data['cols']	= $idcol;
		$data['idrepl'] = $idpembalas;
		$data['pesan'] = $pesan;
		$data['judul'] = "Quotes Lapak ".$th[0]['keterangan'];
		$data['link'] = $th[0]['thread'];
		$data['thr'] = $id;
		$data['isi'] = 'lapak/quotes';
		$data['toket'] = $this->functions->cut_str($page, 'name="securitytoken" value="', '"');
		
		/* --- nav --- */
		$data['wall'] = '<li class="active"><a href="http://cloudmyfile.com/newkaskus/lapak/postThread/'.$id.'">Thread</a></li>';
		$data['compose'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/fjb/'.$id.'">Sundul</a></li>';
		$data['management'] = '<li><a href="http://cloudmyfile.com/newkaskus/lapak/tambahsundul/'.$id.'">Manajemen Otomatis</a></li>';
		
		$this->load->view('lapak/sundul', $data);
	}
	
	public function sendquotes($idpembalas, $cols, $toket, $id){
		$th = $this->jual->threadKaskus($id);
		$thread = $th[0]['thread'];
		
		$post_reply = "http://fjb.kaskus.co.id/post_reply/".$thread;
		$ref= "http://fjb.kaskus.co.id/product/".$thread;
		
		$user = $this->jokes->getUser($idpembalas);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$page = $this->functions->curl($post_reply, $cookie, 0, $ref);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($post_reply, $cookie, 0, $ref);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		unset($post);
		$post = array();
		$post['securitytoken'] = $toket;
		$post['title'] = '';
		$post['message'] = $this->input->post('message');
		$post['ajaxhref'] = '/misc/getsmilies/';
		$post['forumimg'] = '';
		$post['parseurl'] = 1;
		$post['emailupdate'] = '9999';
		$post['folderid'] = 0;
		$post['rating'] = 0;
		$post['sbutton'] = 'Submit Reply';
		
		$page = $this->functions->curl($post_reply, $cookie, $post, $post_reply);
		
		$this->jual->updateStatus($cols);
		redirect('lapak/postThread/'.$id);
	}
	
	function tandaiLapak($id, $thr){
		$this->lapak->updateStatus($id);
		redirect('lapak/postThread/'.$thr);
	}
	
	function tambahLapak(){
		$data['thread'] = $thread = $this->jual->threadAll();
		$data['isi'] = 'lapak/tambahLapak';
		$data['simpan'] = base_url().'lapak/saveLapak/';
		$data['pengirim'] = $this->jual->pengguna();
		$this->load->view('lapak/sundul', $data);
	}
	
	function saveLapak(){
		$thread = $this->input->post('threadnew');
		$pemilik = $this->input->post('pemilik');
		$ket = $this->input->post('keterangan');
		
		preg_match('#\b([\w-]+://?|www[.]|fjb[.])kaskus\.co\.id\/(.+?)\/(.+?)\/#iS', $thread, $match);
		
		$this->jual->saveLapak($match[3], $ket, $pemilik);
		$this->session->set_flashdata('success', 'Data berhasil ditambah');
		redirect('lapak');
	}
	
}
?>