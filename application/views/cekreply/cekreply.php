<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Cyberleech Auto Kaskus</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/style.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-11.2.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap-3.3.4/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/bootstrap-3.3.4/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			$('.strip').dataTable( {
				"bSort" : false
			} );
		} );
	</script>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      		<div class="container">
        		<div class="container-fluid">
				<div class="navbar-header">
				  <a class="navbar-brand" href="#">Otomasi Kaskus</a>
				</div>
				<div>
				  <ul class="nav navbar-nav">
					<li><a href="http://cloudmyfile.com/kaskus/cek_pm">Pesan</a></li>
					<li  class="active"><a href="http://cloudmyfile.com/kaskus/cek_reply">Thread</a></li>
					<li><a href="http://cloudmyfile.com/kaskus/lapakcmf">Lapak CMF</a></li>
					<li><a href="http://cloudmyfile.com/kaskus/lapakcl">Lapak CL</a></li>
					<li><a href="http://cloudmyfile.com/kaskus/lapakpass">Lapak CP</a></li>
					<li><a href="http://cloudmyfile.com/kaskus/lapakflux">Lapak CF</a></li>
					<li><a href="http://cloudmyfile.com/kaskus/admin/user">Pengguna</a></li>
					<li><a href="http://cloudmyfile.com/kaskus/otomasi/logout">LogOut</a></li>
				  </ul>
				</div>
				</div>
			</div>
    </nav>
	
	<div class="container theme-showcase" role="main">
		<?php $this->load->view($isi) ?>
	</div>
<script>
	$('#reset').click(function(){
		$('#linkthread').val("");
	});
	
</script>
</body>
</html>
