<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>
<div class="jumbotron">
		<form method="post" action="<?php echo base_url() ?>admin/simpanSundulCmf">
			<table class="table" border ="0">
			<tr>
				<td align="center">Isi Template Sundul</td><td><textarea name="tempsundul" id="tempsundul" class="form-control" placeholder = "" required></textarea></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><button type="submit" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Save</button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
		</div>
		
		<div class="jumbotron">
		<h4>Daftar Template Sundul</h4>
		<hr>
		<table class='table table-striped table-hover table-bordered table-responsive bordered' id='strip'>
		<thead style='background:#000;color:#fff'>
			<tr>
				<th>Thread Id</th>
				<th>Isi</th>
				<th>status</th>
				<th>Action</th>
			</tr>
		</thead>
			<tbody>
				<?php foreach($sundul as $sn){ ?>
				<tr>
					 <td><?php echo $sn['sundul_cmf_id']; ?></td>
					 <td><?php echo $sn['sundul_cmf_isi']; ?></td>	
					 <td><?php echo $sn['sundul_status']; ?></td>
					 <td><a class="btn btn-primary" href="<?php echo base_url() ?>admin/editSundulCmf/<?php echo $sn['sundul_cmf_id'] ?>"><i class='glyphicon glyphicon-pencil'></i> Edit</a> &nbsp;&nbsp;
						 <a class="btn btn-danger"  href="<?php echo base_url() ?>admin/deleteSundulCmf/<?php echo $sn['sundul_cmf_id'] ?>" onclick="return confirm('anda yakin akan hapus template <?php echo $sn['sundul_cmf_id'] ?> ?')"><i class='glyphicon glyphicon-remove'></i> Delete
					 </td>				 
				</tr>
				<?php } ?>
		
			</tbody>
		</table>
		
		</div>