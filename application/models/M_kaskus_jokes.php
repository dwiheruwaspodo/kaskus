<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Kaskus_jokes extends CI_Model {

	public $db_thread = 'thread_junk';
	
	function getUser($id){
			return $this->db->get_where('user_kaskus', array('user_id'=>$id))->result_array();		
	}
	
	function saveCookie($user, $cookie){
			$data = array(
				'cookie'=> $cookie
			);
			$this->db->where('username', $user);
			$this->db->update('user_kaskus', $data);
	}
	
	function pengguna(){
		$semua = $this->db->query("SELECT * from user_kaskus");
		return $semua->result_array();
	}
	
	public function simpan_semua_alamat($a)
	{
		$simpan = $this->db->query("INSERT IGNORE INTO thread_junk (thread_link, keterangan, status_junk) VALUES ('$a', ' ', 0)");
		return $simpan;	
	}
	
	public function semua_url(){
		return $this->db->select('*')
						->from($this->db_thread)
						->get()
						->result();
	}
	
	public function random_url($a){
		$random = $this->db->query("SELECT thread_id, thread_link FROM thread_junk WHERE status_junk = 0 ORDER BY RAND() LIMIT $a");
		return $random->result_array();
	}
	
	public function random_isijunk($a){
		$random = $this->db->query("SELECT sundul_id, sundul_isi FROM sundul_junk WHERE status_junk = 0 ORDER BY RAND() LIMIT $a");
		return $random->result_array();
	}
	
	public function cari_thread_id($a){
		$cari = $this->db->query("
			SELECT thread_id FROM thread_junk
			WHERE thread_link LIKE '%$a%'
		");
		return $cari->result_array();
	}
	
	function simpanBukti($tgl, $user_id, $thread_id, $hasilnya, $sundul_id){
			$data = array(
				'tanggal'=> $tgl,
				'user_id'=>$user_id,
				'thread_id'=>$thread_id,
				'post_link'=>$hasilnya,
				'sunduljunk_id'=>$sundul_id
			);
			$this->db->insert('bukti_junk', $data);	
	}
	
	function updateStatus($sundul_id){
			$data = array(
				'status_junk'=> 1
			);
			$this->db->where('sundul_id', $sundul_id);
			$this->db->update('sundul_junk', $data);
	}
	
	function updateStatusLink($id_thread){
			$data = array(
				'status_junk'=> 1
			);
			$this->db->where('thread_id', $id_thread);
			$this->db->update('thread_junk', $data);
	}
	
	function ketawa(){
		$random = $this->db->query("SELECT ketawa FROM ketawa ORDER BY RAND()");
		return $random->result_array();
	}
	
	function smile(){
		$random = $this->db->query("SELECT smile FROM smile ORDER BY RAND()");
		return $random->result_array();
	}
	
	function cek_habis(){
		$habis = $this->db->query("SELECT * FROM thread_junk WHERE status_junk = 0");
		return $habis->result_array();
	}
	
	function kehabisan_kata(){
		$habis = $this->db->query("SELECT * FROM sundul_junk WHERE status_junk = 0");
		return $habis->result_array();
	}
	
	function relod_ucapan(){
		$update = $this->db->query("update sundul_junk set status_junk = 0");
		return $update;
	}
}