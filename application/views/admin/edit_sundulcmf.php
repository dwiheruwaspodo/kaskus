<div class="jumbotron">
		<form method="post" action="<?php echo base_url() ?>admin/simpanSundulCmf">
			<table class="table" border ="0">
			<?php foreach($sundul as $sn){ ?>
				<tr>
					<input type="hidden" name="id_sundulcmf" class="form-control" value="<?php echo $sn['sundul_cmf_id'] ?>">
					<td align="center">Isi Template Sundul</td><td><textarea name="tempsundul" id="tempsundul" class="form-control" placeholder = "" required><?php echo $sn['sundul_cmf_isi'] ?></textarea></td>
				</tr>
				<tr>
					<td align="center">status</td>
					<td>
						<select name="status" class="form-control">
							<option value="0" <?php if($sn['sundul_status'] == 0) echo 'selected';?>>0</option>
							<option value="1" <?php if($sn['sundul_status'] == 1) echo 'selected';?>>1</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><button type="submit" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Save</button>&nbsp;&nbsp;
					<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
				</tr>
			<?php } ?>
			</table>
		</form>
		</div>